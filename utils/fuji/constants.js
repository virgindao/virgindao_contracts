// Initial staking index
exports.INITIAL_INDEX = '23636780141';

// First block epoch occurs
exports.FIRST_EPOCH_BLOCK = '1639058400';

// What epoch will be first epoch
exports.FIRST_EPOCH_NUMBER = '362';

// How many blocks are in each epoch ~ 8 hours
exports.EPOCH_LENGTH_IN_BLOCKS = '28800';

// Initial reward rate for epoch
exports.INITIAL_REWARD_RATE = '4035';

// Ethereum 0 address, used when toggling changes in treasury
exports.ZERO_ADDRESS = '0x0000000000000000000000000000000000000000';

// Large number for approval for wAvax and MIM
exports.LARGE_APPROVAL = '100000000000000000000000000000000';

// Initial mint for wAvax and MIM (10,000,000)
exports.INITIAL_MINT = '10000000000000000000000000';

// MIM bond BCV
exports.MIM_BOND_BCV = '188';

// wAvax bond BCV
exports.WAVAX_BOND_BCV = '257';

// Bond vesting length in blocks. 432000 ~ 5 days
exports.BOND_VESTING_LENGTH = '432000';

// Min bond price ~ $1.01
exports.MIN_BOND_PRICE = '101';

// Max bond payout
exports.MAX_BOND_PAYOUT = '75'

// DAO fee for bond ~ 0.1%
exports.BOND_FEE = '10';

// Max debt bond can take on
exports.MAX_BOND_DEBT = '16000000000000000';

// Initial Bond debt
exports.INITIAL_BOND_DEBT = '0';

// AVAX/USD price feed address
exports.AVAX_USD_PRICE_FEED_ADDRESS = '0x5498BB86BC934c8D34FDA08E81D444153d0D06aD';

// WAVAX address
exports.WAVAX_ADDRESS = '0xd00ae08403b9bbb9124bb305c09058e32c39a48c';
