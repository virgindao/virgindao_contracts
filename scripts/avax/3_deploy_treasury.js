const { ethers } = require('hardhat');
const fs = require('fs');
const {
    MIM_ADDRESS
} = require('../../utils/avax/constants');
let address = require('../../utils/avax/address.json');

async function main() {

    const [deployer] = await ethers.getSigners();
    console.log('Deploying contracts with the account: ' + deployer.address);

    // Deploy treasury
    const Treasury = await ethers.getContractFactory('SkadiTreasury');
    const treasury = await Treasury.deploy( address.ski, MIM_ADDRESS, 0 );
    console.log('Treasury deployed on ', treasury.address);

    address['treasury'] = treasury.address;
    fs.writeFileSync('./utils/avax/address.json', JSON.stringify(address));
    console.log('Saved the addresses.');
}

main()
    .then(() => process.exit())
    .catch(error => {
        console.error(error);
        process.exit(1);
})