const { ethers } = require('hardhat');
const fs = require('fs');
let address = require('../../utils/avax/address.json');

async function main() {

    const [deployer] = await ethers.getSigners();
    console.log('Deploying contracts with the account: ' + deployer.address);

    // Deploy sSKI
    const sSki = await ethers.getContractFactory('sSkadi');
    const sski = await sSki.deploy();
    console.log('sSKI deployed on ', sski.address);

    address['sSki'] = sski.address;
    fs.writeFileSync('./utils/avax/address.json', JSON.stringify(address));
    console.log('Saved the addresses.');
}

main()
    .then(() => process.exit())
    .catch(error => {
        console.error(error);
        process.exit(1);
})