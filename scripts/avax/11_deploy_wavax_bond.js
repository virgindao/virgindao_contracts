const { ethers } = require('hardhat');
const fs = require('fs');
const {
    DAO_ADDRESS,
    WAVAX_ADDRESS,
    AVAX_USD_PRICE_FEED_ADDRESS
} = require('../../utils/avax/constants');
let address = require('../../utils/avax/address.json');

async function main() {

    const [deployer] = await ethers.getSigners();
    console.log('Deploying contracts with the account: ' + deployer.address);

    // Deploy WAVAX bond
    const WAvaxBond = await ethers.getContractFactory('JoyAvaxBondDepository');
    const wavaxBond = await WAvaxBond.deploy(address.ski, WAVAX_ADDRESS, address.treasury, DAO_ADDRESS, AVAX_USD_PRICE_FEED_ADDRESS);
    console.log('wAvaxBond deployed on ', wavaxBond.address);

    address['wAvaxBond'] = wavaxBond.address;
    fs.writeFileSync('./utils/avax/address.json', JSON.stringify(address));
    console.log('Saved the addresses.');
}

main()
    .then(() => process.exit())
    .catch(error => {
        console.error(error);
        process.exit(1);
})