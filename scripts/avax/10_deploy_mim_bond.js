const { ethers } = require('hardhat');
const fs = require('fs');
const {
    DAO_ADDRESS,
    MIM_ADDRESS,
    ZERO_ADDRESS
} = require('../../utils/avax/constants');
let address = require('../../utils/avax/address.json');

async function main() {

    const [deployer] = await ethers.getSigners();
    console.log('Deploying contracts with the account: ' + deployer.address);

    // Deploy MIM bond
    const MIMBond = await ethers.getContractFactory('JoyBondDepository');
    const mimBond = await MIMBond.deploy(address.ski, MIM_ADDRESS, address.treasury, DAO_ADDRESS, ZERO_ADDRESS);
    console.log('MimBond deployed on ', mimBond.address);

    address['mimBond'] = mimBond.address;
    fs.writeFileSync('./utils/avax/address.json', JSON.stringify(address));
    console.log('Saved the addresses.');
}

main()
    .then(() => process.exit())
    .catch(error => {
        console.error(error);
        process.exit(1);
})