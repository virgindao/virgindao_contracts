const { ethers } = require('hardhat');
const fs = require('fs');
let address = require('../../utils/avax/address.json');

async function main() {

    const [deployer] = await ethers.getSigners();
    console.log('Deploying contracts with the account: ' + deployer.address);

    // Deploy wSKI
    const wSki = await ethers.getContractFactory('wSKI');
    const wski = await wSki.deploy(address.sSki);
    console.log('wSKI deployed on ', wski.address);

    address['wSki'] = wski.address;
    fs.writeFileSync('./utils/avax/address.json', JSON.stringify(address));
    console.log('Saved the addresses.');
}

main()
    .then(() => process.exit())
    .catch(error => {
        console.error(error);
        process.exit(1);
})