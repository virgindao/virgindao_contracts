const { ethers } = require('hardhat');
const fs = require('fs');
const {
    DAO_ADDRESS
} = require('../../utils/avax/constants');
let address = require('../../utils/avax/address.json');

async function main() {

    const [deployer] = await ethers.getSigners();
    console.log('Deploying contracts with the account: ' + deployer.address);

    // Deploy SKI-MIM bond
    const SkiMimBond = await ethers.getContractFactory('JoyBondDepository');
    const skiMimBond = await SkiMimBond.deploy(address.ski, address.skiMim, address.treasury, DAO_ADDRESS, address.bondingCalc);
    console.log('SkiMimBond deployed on ', skiMimBond.address);

    address['skiMimBond'] = skiMimBond.address;
    fs.writeFileSync('./utils/avax/address.json', JSON.stringify(address));
    console.log('Saved the addresses.');
}

main()
    .then(() => process.exit())
    .catch(error => {
        console.error(error);
        process.exit(1);
})