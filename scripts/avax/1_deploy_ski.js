const { ethers } = require('hardhat');
const fs = require('fs');
let address = new Object();

async function main() {

    const [deployer] = await ethers.getSigners();
    console.log('Deploying contracts with the account: ' + deployer.address);

    // Deploy SKI
    const Ski = await ethers.getContractFactory('SkadiERC20Token');
    const ski = await Ski.deploy();
    console.log('SKI deployed on ', ski.address);

    address['ski'] = ski.address;
    address['skiMim'] = '';
    address['skiAvax'] = '';
    fs.writeFileSync('./utils/avax/address.json', JSON.stringify(address));
    console.log('Saved the addresses.');
}

main()
    .then(() => process.exit())
    .catch(error => {
        console.error(error);
        process.exit(1);
})