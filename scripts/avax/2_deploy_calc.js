const { ethers } = require('hardhat');
const fs = require('fs');
let address = require('../../utils/avax/address.json');

async function main() {

    const [deployer] = await ethers.getSigners();
    console.log('Deploying contracts with the account: ' + deployer.address);

    // Deploy bonding calculator
    const BondingCalc = await ethers.getContractFactory('SkadiBondingCalculator');
    const bondingCalc = await BondingCalc.deploy( address.ski );
    console.log('BondingCalc deployed on ', bondingCalc.address);

    address['bondingCalc'] = bondingCalc.address;
    fs.writeFileSync('./utils/avax/address.json', JSON.stringify(address));
    console.log('Saved the addresses.');
}

main()
    .then(() => process.exit())
    .catch(error => {
        console.error(error);
        process.exit(1);
})