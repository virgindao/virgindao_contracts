const { ethers } = require('hardhat');
const fs = require('fs');
const {
    EPOCH_LENGTH_IN_BLOCKS,
    FIRST_EPOCH_BLOCK
} = require('../../utils/avax/constants');
let address = require('../../utils/avax/address.json');

async function main() {

    const [deployer] = await ethers.getSigners();
    console.log('Deploying contracts with the account: ' + deployer.address);

    // Deploy staking distributor
    const Distributor = await ethers.getContractFactory('Distributor');
    const distributor = await Distributor.deploy(address.treasury, address.ski, EPOCH_LENGTH_IN_BLOCKS, FIRST_EPOCH_BLOCK);
    console.log('Distributor deployed on ', distributor.address);

    address['distributor'] = distributor.address;
    fs.writeFileSync('./utils/avax/address.json', JSON.stringify(address));
    console.log('Saved the addresses.');
}

main()
    .then(() => process.exit())
    .catch(error => {
        console.error(error);
        process.exit(1);
})