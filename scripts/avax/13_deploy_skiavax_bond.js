const { ethers } = require('hardhat');
const fs = require('fs');
const {
    DAO_ADDRESS
} = require('../../utils/avax/constants');
let address = require('../../utils/avax/address.json');

async function main() {

    const [deployer] = await ethers.getSigners();
    console.log('Deploying contracts with the account: ' + deployer.address);

    // Deploy SKI-AVAX bond
    const SkiAvaxBond = await ethers.getContractFactory('JoyBondDepository');
    const skiAvaxBond = await SkiAvaxBond.deploy(address.ski, address.skiAvax, address.treasury, DAO_ADDRESS, address.bondingCalc);
    console.log('SkiAvaxBond deployed on ', skiAvaxBond.address);

    address['skiAvaxBond'] = skiAvaxBond.address;
    fs.writeFileSync('./utils/avax/address.json', JSON.stringify(address));
    console.log('Saved the addresses.');
}

main()
    .then(() => process.exit())
    .catch(error => {
        console.error(error);
        process.exit(1);
})