const { ethers } = require('hardhat');
const fs = require('fs');
const {
    EPOCH_LENGTH_IN_BLOCKS,
    FIRST_EPOCH_NUMBER,
    FIRST_EPOCH_BLOCK
} = require('../../utils/avax/constants');
let address = require('../../utils/avax/address.json');

async function main() {

    const [deployer] = await ethers.getSigners();
    console.log('Deploying contracts with the account: ' + deployer.address);

    // Deploy Staking
    const Staking = await ethers.getContractFactory('SkadiStaking');
    const staking = await Staking.deploy( address.ski, address.sSki, EPOCH_LENGTH_IN_BLOCKS, FIRST_EPOCH_NUMBER, FIRST_EPOCH_BLOCK );
    console.log('Staking deployed on ', staking.address);

    address['staking'] = staking.address;
    fs.writeFileSync('./utils/avax/address.json', JSON.stringify(address));
    console.log('Saved the addresses.');
}

main()
    .then(() => process.exit())
    .catch(error => {
        console.error(error);
        process.exit(1);
})