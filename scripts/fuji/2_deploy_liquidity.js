// @dev. This script will deploy this V1.1 of Skadi. It will deploy the whole ecosystem except for the LP tokens and their bonds. 
// This should be enough of a test environment to learn about and test implementations with the Skadi as of V1.1.
// Not that the every instance of the Treasury's function 'valueOf' has been changed to 'valueOfToken'... 
// This solidity function was conflicting w js object property name

const { ethers } = require('hardhat');
const fs = require('fs');
const {
    ZERO_ADDRESS,
    MIM_BOND_BCV,
    WAVAX_BOND_BCV,
    BOND_VESTING_LENGTH,
    MIN_BOND_PRICE,
    MAX_BOND_PAYOUT,
    BOND_FEE,
    MAX_BOND_DEBT,
    INITIAL_BOND_DEBT
} = require('../../utils/fuji/constants');
let address = require('../../utils/fuji/address.json');

async function main() {

    const [deployer, dao] = await ethers.getSigners();
    console.log('Deploying contracts with the account: ' + deployer.address);

    // Treasury contract
    const Treasury = await ethers.getContractFactory('SkadiTreasury');
    const treasury = await Treasury.attach(address.treasury);

    // Deploy SKI-MIM bond
    //@dev changed function call to Treasury of 'valueOf' to 'valueOfToken' in BondDepository due to change in Treasury contract
    const SKIMIMBond = await ethers.getContractFactory('JoyBondDepository');
    const skiMimBond = await SKIMIMBond.deploy(address.ski, address.skiMimLpToken, address.treasury, deployer.address, address.bondingCalc);
    console.log('SkiMimBond deployed on ', skiMimBond.address);

    // Deploy SKI-AVAX bond
    //@dev changed function call to Treasury of 'valueOf' to 'valueOfToken' in BondDepository due to change in Treasury contract
    const SkiAvaxBond = await ethers.getContractFactory('JoyBondDepository');
    const skiAvaxBond = await SkiAvaxBond.deploy(address.ski, address.skiAvaxLpToken, address.treasury, deployer.address, address.bondingCalc);
    console.log('SkiAvaxBond deployed on ', skiAvaxBond.address);

    // queue and toggle JLP token
    await treasury.queue('5', address.skiMimLpToken);
    await treasury.toggle('5', address.skiMimLpToken, address.bondingCalc);
    await treasury.queue('5', address.skiAvaxLpToken);
    await treasury.toggle('5', address.skiAvaxLpToken, address.bondingCalc);
    console.log('queue and toggle JLP token');

    // queue and toggle LP bond liquidity depositor
    await treasury.queue('4', skiMimBond.address);
    await treasury.toggle('4', skiMimBond.address, ZERO_ADDRESS);
    await treasury.queue('4', skiAvaxBond.address);
    await treasury.toggle('4', skiAvaxBond.address, ZERO_ADDRESS);
    console.log('queue and toggle SKI-MIM and SKI-AVAX bond liquidity depositor');

    // Set LP bond terms
    await skiMimBond.initializeBondTerms(MIM_BOND_BCV, MIN_BOND_PRICE, MAX_BOND_PAYOUT, BOND_FEE, MAX_BOND_DEBT, INITIAL_BOND_DEBT, BOND_VESTING_LENGTH);
    console.log('Set SKI-MIM bond terms');
    await skiAvaxBond.initializeBondTerms(WAVAX_BOND_BCV, MIN_BOND_PRICE, MAX_BOND_PAYOUT, BOND_FEE, MAX_BOND_DEBT, INITIAL_BOND_DEBT, BOND_VESTING_LENGTH);
    console.log('Set SKI-WAVAX bond terms');

    // Set staking for LP bond
    await skiMimBond.setStaking(address.staking, 0);
    await skiMimBond.setStaking(address.stakingHelper, 1);
    await skiAvaxBond.setStaking(address.staking, 0);
    await skiAvaxBond.setStaking(address.stakingHelper, 1);
    console.log('Set staking for LP bond');

    address['skiMimBond'] = skiMimBond.address;
    address['skiAvaxBond'] = skiAvaxBond.address;
    fs.writeFileSync('./utils/fuji/address.json', JSON.stringify(address));
    console.log('Saved the addresses.');

    console.log( 'SkiMimBond: ', skiMimBond.address );
    console.log( 'SkiAvaxBond: ', skiAvaxBond.address );
}

main()
    .then(() => process.exit())
    .catch(error => {
        console.error(error);
        process.exit(1);
})