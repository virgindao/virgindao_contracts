// @dev. This script will deploy this V1.1 of Skadi. It will deploy the whole ecosystem except for the LP tokens and their bonds. 
// This should be enough of a test environment to learn about and test implementations with the Skadi as of V1.1.
// Not that the every instance of the Treasury's function 'valueOf' has been changed to 'valueOfToken'... 
// This solidity function was conflicting w js object property name

const { ethers } = require('hardhat');
const fs = require('fs');
const {
    INITIAL_INDEX,
    FIRST_EPOCH_BLOCK,
    FIRST_EPOCH_NUMBER,
    EPOCH_LENGTH_IN_BLOCKS,
    INITIAL_REWARD_RATE,
    ZERO_ADDRESS,
    LARGE_APPROVAL,
    INITIAL_MINT,
    MIM_BOND_BCV,
    WAVAX_BOND_BCV,
    BOND_VESTING_LENGTH,
    MIN_BOND_PRICE,
    MAX_BOND_PAYOUT,
    BOND_FEE,
    MAX_BOND_DEBT,
    INITIAL_BOND_DEBT,
    AVAX_USD_PRICE_FEED_ADDRESS,
    WAVAX_ADDRESS
} = require('../../utils/fuji/constants');

async function main() {

    const [deployer] = await ethers.getSigners();
    console.log('Deploying contracts with the account: ' + deployer.address);
    //console.log("DAO account: ", dao.address);

    // Deploy MIM
    const MIM = await ethers.getContractFactory('AnyswapV5ERC20');
    const mim = await MIM.deploy('Magic Internet Money', 'MIM', 18, ZERO_ADDRESS, deployer.address);
    console.log('MIM deployed on ', mim.address);

    // Deploy 10,000,000 mock MIM
    await mim.initVault(deployer.address);
    await mim.mint( deployer.address, INITIAL_MINT );
    console.log('MIM minted ', INITIAL_MINT);

    // Deploy JOY
    const JOY = await ethers.getContractFactory('JOYERC20Token');
    const joy = await JOY.deploy();
    console.log('JOY deployed on ', joy.address);

    // Deploy treasury
    //@dev changed function in treaury from 'valueOf' to 'valueOfToken'... solidity function was conflicting w js object property name
    const Treasury = await ethers.getContractFactory('JOYTreasury');
    const treasury = await Treasury.deploy( joy.address, mim.address, 0 );
    console.log('Treasury deployed on ', treasury.address);

    // Deploy bonding calc
    const BondingCalc = await ethers.getContractFactory('JOYBondingCalculator');
    const bondingCalc = await BondingCalc.deploy( joy.address );
    console.log('JOYBondingCalculator deployed on ', bondingCalc.address);

    // Deploy staking distributor
    const Distributor = await ethers.getContractFactory('Distributor');
    const distributor = await Distributor.deploy(treasury.address, joy.address, EPOCH_LENGTH_IN_BLOCKS, FIRST_EPOCH_BLOCK);
    console.log('Distributor deployed on ', distributor.address);

    // Deploy nJOY
    const nJOY = await ethers.getContractFactory('nJOY');
    const njoy = await nJOY.deploy();
    console.log('nJOY deployed on ', njoy.address);

    // Deploy wJOY
    const rJOY = await ethers.getContractFactory('rJOY');
    const rjoy = await rJOY.deploy(njoy.address);
    console.log('rJOY deployed on ', rjoy.address);

    // Deploy Staking
    const Staking = await ethers.getContractFactory('JOYStaking');
    const staking = await Staking.deploy( joy.address, njoy.address, EPOCH_LENGTH_IN_BLOCKS, FIRST_EPOCH_NUMBER, FIRST_EPOCH_BLOCK );
    console.log('JOYStaking deployed on ', staking.address);

    // Deploy staking warmpup
    const StakingWarmpup = await ethers.getContractFactory('StakingWarmup');
    const stakingWarmup = await StakingWarmpup.deploy(staking.address, njoy.address);
    console.log('StakingWarmup deployed on ', stakingWarmup.address);

    // Deploy staking helper
    const StakingHelper = await ethers.getContractFactory('StakingHelper');
    const stakingHelper = await StakingHelper.deploy(staking.address, joy.address);
    console.log('StakingHelper deployed on ', stakingHelper.address);

    // Initialize njoy and set the index
    await njoy.initialize(staking.address);
    await njoy.setIndex(INITIAL_INDEX);
    console.log('Initialize nJOY and set the index');

    // set distributor contract and warmup contract
    await staking.setContract('0', distributor.address);
    await staking.setContract('1', stakingWarmup.address);
    console.log('Set distributor contract and warmup contract');

    // Set treasury for JOY token
    await joy.setVault(treasury.address);
    console.log('Set treasury for JOY token');

    // Add staking contract as distributor recipient
    await distributor.addRecipient(staking.address, INITIAL_REWARD_RATE);
    console.log('Add staking contract as distributor recipient');

    // Queue and toggle reward manager
    await treasury.queue('8', distributor.address);
    console.log('queue reward manager');
    await treasury.toggle('8', distributor.address, ZERO_ADDRESS);
    console.log('toggle reward manager');

    // Queue and toggle deployer reserve depositor
    await treasury.queue('0', deployer.address);
    console.log('queue deployer reserve depositor');
    await treasury.toggle('0', deployer.address, ZERO_ADDRESS);
    console.log('toggle deployer reserve depositor');

    // Queue and toggle liquidity depositor
    await treasury.queue('4', deployer.address, );
    console.log('queue liquidity depositor');
    await treasury.toggle('4', deployer.address, ZERO_ADDRESS);
    console.log('toggle liquidity depositor');

    // Approve the treasury to spend MIM
    await mim.approve(treasury.address, LARGE_APPROVAL );
    console.log('Approved the treasury to spend MIM');

    // Approve staking and staking helper contact to spend deployer's JOY
    await joy.approve(staking.address, LARGE_APPROVAL);
    await joy.approve(stakingHelper.address, LARGE_APPROVAL);
    console.log('JOY approved StakingHelper');

    // Deposit 9,000,000 MIM to treasury, 600,000 JOY gets minted to deployer and 8,400,000 are in treasury as excesss reserves
    await treasury.deposit('9000000000000000000000000', mim.address, '8400000000000000');
    console.log('Treasury deposited MIM');

    // Stake JOY through helper
    await stakingHelper.stake('100000000000', deployer.address);
    console.log('Staked JOY through helper');

    // Deploy MIM bond
    //@dev changed function call to Treasury of 'valueOf' to 'valueOfToken' in BondDepository due to change in Treasury contract
    const MIMBond = await ethers.getContractFactory('JoyBondDepository');
    const mimBond = await MIMBond.deploy(joy.address, mim.address, treasury.address, deployer.address, ZERO_ADDRESS);
    console.log('MimBond deployed on ', mimBond.address);

    // Deploy WAVAX bond
    //@dev changed function call to Treasury of 'valueOf' to 'valueOfToken' in BondDepository due to change in Treasury contract
    const WAvaxBond = await ethers.getContractFactory('JoyAvaxBondDepository');
    const wavaxBond = await WAvaxBond.deploy(joy.address, WAVAX_ADDRESS, treasury.address, deployer.address, AVAX_USD_PRICE_FEED_ADDRESS);
    console.log('wAvaxBond deployed on ', wavaxBond.address);

    // Queue and toggle nJOY token
    await treasury.queue('9', njoy.address);
    await treasury.toggle('9', njoy.address, ZERO_ADDRESS);
    console.log('Queue and toggle nJOY token');

    // Queue and toggle MIM and WAVAX reserve token
    await treasury.queue('2', mim.address);
    await treasury.toggle('2', mim.address, ZERO_ADDRESS);
    await treasury.queue('2', WAVAX_ADDRESS);
    await treasury.toggle('2', WAVAX_ADDRESS, ZERO_ADDRESS);
    console.log('Queue and toggle MIM and WAVAX reserve token');

    // Approve mim bonds to spend deployer's MIM
    await mim.approve(mimBond.address, LARGE_APPROVAL );
    console.log('Approved mim bond to spend deployer`s MIM');

    // Queue and toggle MIM bond reserve depositor
    await treasury.queue('0', mimBond.address);
    await treasury.toggle('0', mimBond.address, ZERO_ADDRESS);
    console.log('Queue and toggle MIM bond reserve depositor');

    // Queue and toggle WAVAX bond reward manager
    await treasury.queue('8', wavaxBond.address);
    await treasury.toggle('8', wavaxBond.address, ZERO_ADDRESS);
    console.log('Queue and toggle WAVAX bond reward manager');

    // Set MIM bond terms
    await mimBond.initializeBondTerms(MIM_BOND_BCV, MIN_BOND_PRICE, MAX_BOND_PAYOUT, BOND_FEE, MAX_BOND_DEBT, INITIAL_BOND_DEBT, BOND_VESTING_LENGTH);
    console.log('Set MIM bond terms');
    await wavaxBond.initializeBondTerms(WAVAX_BOND_BCV, MIN_BOND_PRICE, MAX_BOND_PAYOUT, MAX_BOND_DEBT, INITIAL_BOND_DEBT, BOND_VESTING_LENGTH);
    console.log('Set WAVAX bond terms');

    // Set staking for MIM bond
    await mimBond.setStaking(staking.address, 0);
    await mimBond.setStaking(stakingHelper.address, 1);
    await wavaxBond.setStaking(staking.address, 0);
    await wavaxBond.setStaking(stakingHelper.address, 1);
    console.log('Set staking for MIM bond');

    const address = {
        mim: mim.address,
        joy: joy.address,
        nJOY: njoy.address,
        rJOY: rjoy.address,
        treasury: treasury.address,
        bondingCalc: bondingCalc.address,
        distributor: distributor.address,
        staking: staking.address,
        stakingWarmup: stakingWarmup.address,
        stakingHelper: stakingHelper.address,
        mimBond: mimBond.address,
        wAvaxBond: wavaxBond.address,
        joyMimLpToken: '',
        joyAvaxLpToken: ''
    };
    fs.writeFileSync('./utils/fuji/address_virgindao.json', JSON.stringify(address));
    console.log('Saved the addresses.');

    console.log( 'MIM: ' + mim.address );
    console.log( 'JOY: ' + joy.address );
    console.log( 'nJOY: ' + njoy.address );
    console.log( 'rJOY: ' + rjoy.address );
    console.log( 'JOYTreasury: ' + treasury.address );
    console.log( 'JOYBondingCalc: ' + bondingCalc.address );
    console.log( 'Distributor: ' + distributor.address );
    console.log( 'JOYStaking: ' + staking.address );
    console.log( 'StakingWarmup: ' + stakingWarmup.address );
    console.log( 'StakingHelper: ' + stakingHelper.address );
    console.log( 'MimBond: ' + mimBond.address );
    console.log( 'wAvaxBond: ' + wavaxBond.address );
}

main()
    .then(() => process.exit())
    .catch(error => {
        console.error(error);
        process.exit(1);
})