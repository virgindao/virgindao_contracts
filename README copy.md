Compiling 1 file with 0.5.17
Compiling 11 files with 0.7.5
Compiling 1 file with 0.8.2
Compilation finished successfully
Deploying contracts with the account: 0xA7dEF2DF5e70aC66Ab414B958F4dc70Bce49e2bf
MIM deployed on  0xfA26E1639A41F2627DC7078E39069f518099956F
MIM minted  10000000000000000000000000
JOY deployed on  0x52C97723793623A41027d5abc6dF55557649d976
Treasury deployed on  0xD4A11A57650c45D4e71EDF506A15b01dd3839764
JOYBondingCalculator deployed on  0xEDd97b3866e6872f9901bc9971CfaC29808C7283
Distributor deployed on  0x6552CDE8Fcd60912AA4C99cc0ED1cC800574d8B8
nJOY deployed on  0x3c546270b27d48f6032a7132CcfE964c827Dc8a3
rJOY deployed on  0x0bBAE72954388faFd3Ca12aE7963af458DE4C24c
JOYStaking deployed on  0xB3451dbfEb3aee093FE0d13E235f2a463DbdfC09
StakingWarmup deployed on  0xb2EE093F5907E514AB3bF7005A844C98c83BbDA9
StakingHelper deployed on  0x99Cd48960ec7EfF29151c6b3797C55B0D82C592f
Initialize nJOY and set the index
Set distributor contract and warmup contract
Set treasury for JOY token
Add staking contract as distributor recipient
queue reward manager
toggle reward manager
queue deployer reserve depositor
toggle deployer reserve depositor
queue liquidity depositor
toggle liquidity depositor
Approved the treasury to spend MIM
JOY approved StakingHelper
Treasury deposited MIM
Staked JOY through helper
MimBond deployed on  0x7AdCeF899A863acE1C590e2FFce48aD1704865d5
wAvaxBond deployed on  0x4FD2FAe93B0c42e638D94EBBA02939e1a42dd50a
Queue and toggle nJOY token
Queue and toggle MIM and WAVAX reserve token
Approved mim bond to spend deployer`s MIM
Queue and toggle MIM bond reserve depositor
Queue and toggle WAVAX bond reward manager
Set MIM bond terms
Set WAVAX bond terms
Set staking for MIM bond
Saved the addresses.
MIM: 0xfA26E1639A41F2627DC7078E39069f518099956F
JOY: 0x52C97723793623A41027d5abc6dF55557649d976
nJOY: 0x3c546270b27d48f6032a7132CcfE964c827Dc8a3
rJOY: 0x0bBAE72954388faFd3Ca12aE7963af458DE4C24c
JOYTreasury: 0xD4A11A57650c45D4e71EDF506A15b01dd3839764
JOYBondingCalc: 0xEDd97b3866e6872f9901bc9971CfaC29808C7283
Distributor: 0x6552CDE8Fcd60912AA4C99cc0ED1cC800574d8B8
JOYStaking: 0xB3451dbfEb3aee093FE0d13E235f2a463DbdfC09
StakingWarmup: 0xb2EE093F5907E514AB3bF7005A844C98c83BbDA9
StakingHelper: 0x99Cd48960ec7EfF29151c6b3797C55B0D82C592f
MimBond: 0x7AdCeF899A863acE1C590e2FFce48aD1704865d5
wAvaxBond: 0x4FD2FAe93B0c42e638D94EBBA02939e1a42dd50a