// @dev. This script will deploy this V1.1 of Skadi. It will deploy the whole ecosystem except for the LP tokens and their bonds. 
// This should be enough of a test environment to learn about and test implementations with the Skadi as of V1.1.
// Not that the every instance of the Treasury's function 'valueOf' has been changed to 'valueOfToken'... 
// This solidity function was conflicting w js object property name

const { ethers } = require('hardhat');
const fs = require('fs');
 

async function main() {

    const [deployer, dao] = await ethers.getSigners();
    console.log(' contracts with the account: ' + deployer.address);
    let joy = await  ethers.getContractAt("JOYERC20Token", "0x52C97723793623A41027d5abc6dF55557649d976");
    let mim = await  ethers.getContractAt("AnyswapV5ERC20", "0xfA26E1639A41F2627DC7078E39069f518099956F");
    let nJOY  = await  ethers.getContractAt("nJOY", "0x3c546270b27d48f6032a7132CcfE964c827Dc8a3");
    let jOYStaking  = await  ethers.getContractAt("JOYStaking", "0xB3451dbfEb3aee093FE0d13E235f2a463DbdfC09");
    let jOYDistributor  = await  ethers.getContractAt("Distributor", "0x6552CDE8Fcd60912AA4C99cc0ED1cC800574d8B8");

    console.log("Deployer Details") ; 
   

    console.log(' contracts with the account: ' + dao.address);


    const joyBalance = await joy.balanceOf(deployer.address);
    const mimBalance = await mim.balanceOf(deployer.address);
    const nJOYBalance = await nJOY.balanceOf(dao.address);


    console.log("joyBalance:"+joyBalance/10**9) 
    console.log("mimBalance:"+mimBalance/10**18) 
    console.log("nJOYBalance:"+nJOYBalance/10**9) 

    console.log("epoch.number:"+ await jOYStaking.getEpochNumber()) 
    console.log("epoch.distribute:"+await jOYStaking.getEpochD()/10**18) 
    console.log("epoch.endTime:"+await jOYStaking.getEpochEndTime()) 
    console.log("block.timestamp:"+await jOYStaking.getBlockTs()) 
    console.log("Index :"+await jOYStaking.index()) 

    console.log("nextRewardFor :"+await jOYDistributor.nextRewardFor(jOYStaking.address)/100000) 

    console.log("rebasing now :"+ await jOYStaking.rebase()) 


    console.log("epoch.number:"+ await jOYStaking.getEpochNumber()) 
    console.log("epoch.distribute:"+await jOYStaking.getEpochD()/10**18) 
    console.log("epoch.endTime:"+await jOYStaking.getEpochEndTime()) 
    console.log("block.timestamp:"+await jOYStaking.getBlockTs()) 
    console.log("Index :"+await jOYStaking.index()) 


    console.log("nJOYBalance  :"+await nJOY.balanceOf(dao.address)/10**9); 


    console.log("contractBalance :"+await jOYStaking.contractBalance()/10**9) 
    //console.log("circulatingSupply :"+await jOYStaking.circulatingSupply()/10**9) 

}

main()
    .then(() => process.exit())
    .catch(error => {
        console.error(error);
        process.exit(1);
})