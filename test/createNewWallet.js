// @dev. This script will deploy this V1.1 of Skadi. It will deploy the whole ecosystem except for the LP tokens and their bonds. 
// This should be enough of a test environment to learn about and test implementations with the Skadi as of V1.1.
// Not that the every instance of the Treasury's function 'valueOf' has been changed to 'valueOfToken'... 
// This solidity function was conflicting w js object property name

const { ethers } = require('hardhat');
const fs = require('fs');
require("@nomiclabs/hardhat-ethers");


async function main() {

    const wallet = ethers.Wallet.createRandom(); 

 
    console.log("wallet address  "+ wallet.address) ; 
    console.log("wallet    "+ wallet.privateKey) ; 
    console.log("wallet    "+ wallet.mnemonic.phrase) ; 
   

    const provider = ethers.provider;

    console.log("provider    "+ await ethers.provider.getBlockNumber() ); 

    const Walle = wallet.connect(ethers.provider);
    const MIM = await ethers.getContractFactory('AnyswapV5ERC20',Walle);
     
    console.log('MIM deployed on ',10000*25*10);





}

main()
    .then(() => process.exit())
    .catch(error => {
        console.error(error);
        process.exit(1);
})